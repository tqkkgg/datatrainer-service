from engine import Database
from sqlalchemy import text
import requests
import json


ES_URL = "http://es-staging.parentstory.com"
ES_INDEX = "recommendations"


def run_indexer(product_type='activity'):
    products = fetch_products(product_type=product_type)
    actions = []
    deletedIds = []

    insert_url = f"{ES_URL}/_bulk/"
    delete_url = f"{ES_URL}/{product_type}/_delete_by_query"

    headers = {
        "Content-Type": "application/x-ndjson"
    }

    for product in products:
        if product.deleted_at is not None:
            deletedIds.append(product.global_id)
            continue

        action = {"index": {"_index": product_type,
                            "_type": "listing", "_id": str(product.global_id)}}

        searchable_values = product.searchable_values.split(",") if product.searchable_values else []
        related_values = product.related_values.split(",") if product.related_values else []
        filterable_keys = product.filterable_keys.split(",") if product.filterable_keys else []
        filterable_values = product.filterable_values.split(",") if product.filterable_values else []

        data = {
            "id": int(product.global_id),
            "searchables": searchable_values,
            "related": related_values,
            "start_date": product.start_date.strftime("%Y-%m-%d") if product.start_date else None,
            "end_date": product.end_date.strftime("%Y-%m-%d") if product.end_date else None
        }

        for idx, key in enumerate(filterable_keys):
            if (key not in data):
                data[key] = []

            if filterable_values[idx] not in data[key]:
                data[key].append(filterable_values[idx])

        actions.append(json.dumps(action))
        actions.append(json.dumps(data))

        if len(actions) == 200:
            actions_string = "\n".join(actions) + "\n"
            actions = []

            requests.post(insert_url, headers=headers, data=actions_string)

    if len(actions) > 0:
        actions_string = "\n".join(actions) + "\n"
        actions = []

        res = requests.post(insert_url, headers=headers, data=actions_string)
        print(res.content)

    if len(deletedIds) > 0:

        query = json.dumps({
            "query": {
                "terms": {
                    "_id": deletedIds
                }
            }
        })

        res = requests.post(delete_url, headers=headers, data=query)
        print(res.content)


def fetch_products(product_type: str):
    query = """
        SELECT 
            p.*,
            GROUP_CONCAT(pf.filter_key) as filterable_keys,
            GROUP_CONCAT(pf.value) as filterable_values,
            GROUP_CONCAT(DISTINCT(pk.value)) as searchable_values,
            f.related_values
        FROM products p
        LEFT JOIN product_filters pf ON p.id=pf.product_id
        LEFT JOIN product_keywords pk ON p.id=pk.product_id
        LEFT JOIN ( 
            SELECT pr.product_id, pr.foreign_id, GROUP_CONCAT(DISTINCT(pk2.value)) as related_values
            FROM product_keywords pk2
            JOIN product_relations pr ON pr.foreign_id=pk2.product_id 
            GROUP BY pr.product_id
        ) f ON f.product_id = p.id
        WHERE product_type = :product_type
        GROUP BY p.global_id
    """

    with Database().transaction() as session:
        rows = session.execute(text(query), dict(
            product_type=product_type
        )).fetchall()

    return rows
