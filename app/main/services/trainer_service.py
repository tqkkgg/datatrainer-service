from main.main import db
import pandas as pd
import numpy as np
from scipy.sparse import csr_matrix
import requests
import json


ES_URL = "http://es-staging.parentstory.com"
ES_INDEX = "recommendations"


def run_trainer(event_type='view', product_type='activity'):
    query = """
        SELECT *
        FROM analytics
        WHERE event_type = %(event_type)s
        AND product_type = %(product_type)s
    """

    df = pd.read_sql_query(query, db.engine, params={
        'event_type': event_type,
        'product_type': product_type
    })

    visitors = df['user_id'].unique()
    items = df['product_id'].unique()

    analytics = df.groupby(['user_id']).head(50)

    analytics['visitors'] = analytics['user_id'].apply(
        lambda x: np.argwhere(visitors == x)[0][0])
    analytics['items'] = analytics['product_id'].apply(
        lambda x: np.argwhere(items == x)[0][0])

    occurences = csr_matrix((visitors.shape[0], items.shape[0]), dtype='int8')
    popular = np.zeros(items.shape[0])

    def set_occurences(visitor, item):
        occurences[visitor, item] += 1

    def inc_popular(index):
        popular[index] += 1

    analytics.apply(lambda row: set_occurences(
        row['visitors'], row['items']), axis=1)
    analytics.apply(lambda row: inc_popular(row['items']), axis=1)

    cooc = occurences.transpose().dot(occurences)
    cooc.setdiag(0)

    row_sum = np.sum(cooc, axis=0).A.flatten()
    column_sum = np.sum(cooc, axis=1).A.flatten()
    total = np.sum(row_sum, axis=0)
    pp_score = csr_matrix((cooc.shape[0], cooc.shape[1]), dtype='double')
    cx = cooc.tocoo()

    for i, j, v in zip(cx.row, cx.col, cx.data):
        if v != 0:
            k11 = v
            k12 = row_sum[i] - k11
            k21 = column_sum[j] - k11
            k22 = total - k11 - k12 - k21
            pp_score[i, j] = rootLLR(k11, k12, k21, k22)

    result = np.flip(np.sort(pp_score.A, axis=1), axis=1)
    result_indices = np.flip(np.argsort(pp_score.A, axis=1), axis=1)

    minLLR = 1
    indicators = result[:, :50]
    indicators[indicators < minLLR] = 0.0
    indicators_indices = result_indices[:, :50]
    max_indicator_indices = (indicators == 0).argmax(axis=1)
    max = max_indicator_indices.max()
    indicators = indicators[:, :max+1]
    indicators_indices = indicators_indices[:, :max+1]

    actions = []

    for i in range(indicators.shape[0]):
        length = indicators[i].nonzero()[0].shape[0]
        real_indicators = items[indicators_indices[i, :length]].astype(
            "int").tolist()
        id = items[i]

        action = {"index": {"_index": ES_INDEX,
                            "_type": product_type, "_id": str(id)}}

        data = {
            "id": int(id),
            "indicators": real_indicators,
            "popular": popular[i]
        }

        actions.append(json.dumps(action))
        actions.append(json.dumps(data))

        if len(actions) == 200:
            actions_string = "\n".join(actions) + "\n"
            actions = []

            url = f"{ES_URL}/_bulk/"
            headers = {
                "Content-Type": "application/x-ndjson"
            }
            requests.post(url, headers=headers, data=actions_string)

    if len(actions) > 0:
        actions_string = "\n".join(actions) + "\n"
        actions = []
        url = f"{ES_URL}/_bulk/"
        headers = {
            "Content-Type": "application/x-ndjson"
        }
        res = requests.post(url, headers=headers, data=actions_string)
        print(res.content)


def xLogX(x):
    return x * np.log(x) if x != 0 else 0.0


def entropy(x1, x2=0, x3=0, x4=0):
    return (
        xLogX(x1 + x2 + x3 + x4) 
        - xLogX(x1) - xLogX(x2) - xLogX(x3) - xLogX(x4)
    )


def LLR(k11, k12, k21, k22):
    rowEntropy = entropy(k11 + k12, k21 + k22)
    columnEntropy = entropy(k11 + k21, k12 + k22)
    matrixEntropy = entropy(k11, k12, k21, k22)

    if rowEntropy + columnEntropy < matrixEntropy:
        return 0.0

    return 2.0 * (rowEntropy + columnEntropy - matrixEntropy)


def rootLLR(k11, k12, k21, k22):
    llr = LLR(k11, k12, k21, k22)
    sqrt = np.sqrt(llr)

    if k11 * 1.0 / (k11 + k12) < k21 * 1.0 / (k21 + k22):
        sqrt = -sqrt

    return sqrt
