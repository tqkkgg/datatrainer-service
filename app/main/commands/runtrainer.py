from flask_script import Command, Manager, Option
from main.services.trainer_service import run_trainer

class RunTrainerCommand(Command):

    option_list = [
        Option('--event', '-e', dest='event'),
        Option('--product', '-p', dest='product')
    ]

    def run(self, event='view', product='activity'):
        print(f"user {event} {product}")
        run_trainer(event_type=event, product_type=product)