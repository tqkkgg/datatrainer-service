from flask_script import Command, Manager, Option
from main.services.indexer_service import run_indexer

class RunIndexerCommand(Command):

    option_list = [
        Option('--product', '-p', dest='product')
    ]

    def run(self, product='activity'):
        run_indexer(product_type=product)