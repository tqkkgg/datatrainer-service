from flask import Flask, Response
from engine import CoreEngine, config, logger, Database, MockResponse, http, secret_manager
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
import os
import json


app = Flask(__name__)


CORS(app)

if config.get('secret_manager', 'name'):
    secret_manager.load_to_env(
        config.get('secret_manager', 'name'),
        config.get('secret_manager', 'access_key'),
        config.get('secret_manager', 'secret_key')
    )


CoreEngine(app, app_name=config.get('app', 'name'))

app.config['SECRET_KEY'] = config.get("app", "secret_key")
app.config['SQLALCHEMY_DATABASE_URI'] = config.get("database", "uri")
app.config['SQLALCHEMY_POOL_SIZE'] = config.get_int("database", "pool_size")
app.config['SQLALCHEMY_POOL_TIMEOUT'] = config.get_int("database", "pool_timeout")
app.config['SQLALCHEMY_POOL_RECYCLE'] = config.get_int("database", "pool_recycle")
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False


db = SQLAlchemy(app)
Database(db=db)


@app.route("/ms/datascalper/c/health", methods=["GET"])
def hello():
    return json.dumps({
        'build': os.environ.get('CI_BUILD_NUMBER', '-')
    }), 200, {
        'Cache-Control': 'private',
        'Content-Type': 'application/json'
    }


# blueprint from api
from main.api.v1 import blueprint as api_v1

app.register_blueprint(api_v1)
