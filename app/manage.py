from flask import Flask
from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager
from flask_sqlalchemy import SQLAlchemy
from engine import config, secret_manager
from main.commands.runtrainer import RunTrainerCommand
from main.commands.runindexer import RunIndexerCommand

app = Flask(__name__)

if config.get('secret_manager', 'name'):
    secret_manager.load_to_env(
        config.get('secret_manager', 'name'),
        config.get('secret_manager', 'access_key'),
        config.get('secret_manager', 'secret_key')
    )

app.config['SECRET_KEY'] = config.get("app", "secret_key")
app.config['SQLALCHEMY_DATABASE_URI'] = config.get("database", "uri")
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)
migrate = Migrate(app, db)

manager = Manager(app)
manager.add_command('db', MigrateCommand)
manager.add_command('runtrainer', RunTrainerCommand)
manager.add_command('runindexer', RunIndexerCommand)

if __name__ == '__main__':
    manager.run()
